package com.example.course.intercom;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("user.service")
public interface UserClient {

  @GetMapping(value="services/names")
    List<String>getUserNames(@RequestBody List<Long> UserIdList);

}