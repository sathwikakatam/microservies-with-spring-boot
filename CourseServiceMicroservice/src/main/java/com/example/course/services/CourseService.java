package com.example.course.services;

import com.example.course.model.Transactional;
import com.example.course.model.course;
import java.util.List;



    public interface CourseService {
        List<course> allCourses();
    
        course findCourseById(Long courseId);
    
        List<Transactional> findTransactionsOfUser(Long userId);
    
        List<Transactional> findTransactionsOfCourse(Long courseId);
    
        Transactional saveTransaction(Transactional transaction);
}