package com.example.course.services;

import java.util.List;

import com.example.course.model.Transactional;
import com.example.course.model.course;
import com.example.course.repositories.CourseRepository;
import com.example.course.repositories.TransactionalRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private TransactionalRepository transactionRepository;

    public List<course> allCourses() {
        // TODO Auto-generated method stub
        return courseRepository.findAll();
    }

    public course findCourseById(Long courseId) {
        // TODO Auto-generated method stub
        return courseRepository.findById(courseId).orElse(null);
    }

    public List<Transactional> findTransactionsOfUser(Long userId) {
        // TODO Auto-generated method stub
        return transactionRepository.findByuserId(userId);
    }

    public List<Transactional> findTransactionsOfCourse(Long courseId) {
        // TODO Auto-generated method stub
        return transactionRepository.findByCourseId(courseId);
    }

    public Transactional saveTransaction(Transactional transaction) {
        // TODO Auto-generated method stub
        return transactionRepository.save(transaction);
    }

   
}