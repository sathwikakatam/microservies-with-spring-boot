package com.example.course.repositories;

import java.util.List;

import com.example.course.model.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionalRepository extends JpaRepository<Transactional,Long> {
   
    List<Transactional>findByuserId(Long id);

    List<Transactional>findByCourseId(Long id);
}