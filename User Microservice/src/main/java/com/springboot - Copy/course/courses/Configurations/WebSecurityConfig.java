package com.springboot.course.courses.Configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebSecurity
@Configuration
@Order(1)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailsService userservice;

   @Override
   protected void configure(AuthenticationManagerBuilder auth) throws Exception {
     auth.userDetailsService(userservice).passwordEncoder(passwordencoder()); 
}

@Bean
public PasswordEncoder passwordencoder(){
    return new BCryptPasswordEncoder();
}

@Bean
public WebMvcConfigurer csrfConfig(){
    return new WebMvcConfigurer() {
    @Override
    public void addCorsMappings(CorsRegistry registry){
          registry.addMapping("/**").allowedOrigins("*");
    }
    };
}





    @Override
    protected void configure(HttpSecurity http) throws Exception{
           http.cors() 
           .and().authorizeRequests().antMatchers("/h2", "/error","/service/**").permitAll()
           .anyRequest().fullyAuthenticated().and().logout().permitAll()
           .logoutRequestMatcher(new AntPathRequestMatcher("/service/logout","POST")).
              and()
           .formLogin().loginPage("/service/login")
    .and()
    .httpBasic().and().csrf().disable();
    }
    }

