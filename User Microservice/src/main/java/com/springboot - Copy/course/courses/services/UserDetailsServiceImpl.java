package com.springboot.course.courses.services;

import java.util.HashSet;
import java.util.Set;

import com.springboot.course.courses.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserInterface ui;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       User user = ui.findByUsername(username);
        if(user==null){
            throw new UsernameNotFoundException(username);
        }
        Set<GrantedAuthority>authorities =new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole().name()));
        
        return new org.springframework.security.core.userdetails.User(username,user.getPassword(),authorities);
            }
        
    }



