package com.springboot.course.courses.repositories;

import java.util.List;
import java.util.Optional;

import com.springboot.course.courses.model.User;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

   Optional<User> findByusername(String id) ;
   
  @Query("select u.name from User u where u.id in (:pIdList)")
   List<String>findByList(@Param("pIdList") List<Long>p_idlist);

    
}