package com.springboot.course.courses.controllers;

import java.security.Principal;
import java.util.List;

import com.springboot.course.courses.model.User;
import com.springboot.course.courses.services.UserInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class UserController {

    @Autowired
    UserInterface ui;

    //Autowire discoveryClient
    @Autowired
    private DiscoveryClient discoveryclient;

    @Autowired
    private Environment env;

    @Value("${spring.application.name}")
    private String serviceId;

    //create method to get  eureka services

   // @GetMapping("/service/services")
    // public RequestEntity<?> getServices(){
    //     return new ResponseEntity<>(discoveryclient.getServi,HttpStatus.OK);
        
    // }
    //create method to get port number
    @GetMapping("/service/port")
    public String getPort(){
        return (env.getProperty("local.server.port"));
    }
   

    // create method to get instaces

    @GetMapping("/service/instances")
    public ResponseEntity<?> getInstance(){

        return new ResponseEntity<>(discoveryclient.getInstances(serviceId),HttpStatus.OK);
    }

    


    @PostMapping("/service/register")
    public ResponseEntity<?> registration(@RequestBody final User user) {
        // check if the user name exists or not if not return conflict
        // status code of conflict 409
        if (ui.findByUsername(user.getUsername()) != null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        ui.save(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/service/login")
    public ResponseEntity<?> logIn(final Principal principal) {
        //principal is a javasecurity object which is send by client and through which can extract user name and passwaord
        if(principal==null|| principal.getName()==null){

            //if the user logged out login?logout
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return ResponseEntity.ok(ui.findByUsername(principal.getName()));
    }

    
    @PostMapping("/service/names")
    public ResponseEntity<?> getNamesbyId(@RequestBody final List<Long> list){
        return ResponseEntity.ok(ui.findusers(list));

    }

    @GetMapping("/service/test")
    public ResponseEntity<?> test(){
        return ResponseEntity.ok("workingg.....");


    }

}