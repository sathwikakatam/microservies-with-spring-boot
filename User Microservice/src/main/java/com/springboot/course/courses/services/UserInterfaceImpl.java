package com.springboot.course.courses.services;

import java.util.List;

import com.springboot.course.courses.model.User;
import com.springboot.course.courses.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;



@Service
public class UserInterfaceImpl implements UserInterface {

    @Autowired
    private UserRepository user_repo;

    // @Autowired
    // private PasswordEncoder passwordencoder;

    @Override
    public User save(User user) {
        user.setPassword(user.getPassword());
        return user_repo.save(user);
    }

    @Override
    public User findByUsername(String username) {
       
        return user_repo.findByusername(username).orElse(null);
    }

    @Override
    public List<String> findusers(List<Long> id_list) {
        
       return user_repo.findByList(id_list);
      
    }

    




}