package com.springboot.course.courses.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

  

@Entity
@Table(name="user")
public class User {

     @Id
    @Column(name ="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long name;

    @Column(name="username")
    private String username;
    

    @Column(name="password")
    private String password;

   @Enumerated(value=EnumType.STRING)
    @Column(name="role")
    private Role role;

    public long getId() {
        return name;
    }

    public void setId(long id) {
        this.name = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

// public CharSequence getPassword() {
// 	return null;
// }

// public void setPassword(String encode) {
// }

// public String getUsername() {
// 	return null;
// }
}