package com.springboot.course.courses.services;

import java.util.List;

import com.springboot.course.courses.model.User;

public interface UserInterface {
public User save(User user);
public User findByUsername(String username);
public List<String> findusers(List<Long>id_list);

}